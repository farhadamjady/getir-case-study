# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Base URL

http://ec2-18-192-4-124.eu-central-1.compute.amazonaws.com:5000

## Installation

```bash
$ npm install
```

## Running the app

In order to configurate environment variables make your own `.env` file based on `.env.example`

```bash
# development
$ npm start
```

## Test

```bash
# unit tests
$ npm run test
```

## API Reference

### Filter Records

Get the records filtered based on input.

**URL** : `/records`

**Method** : `POST`

**Data Params** :
```json
{
    "minCount": "The minimum count of total amount (sum of count array).[format=integer]",
    "maxCount": "The maximum count of total amount (sum of count array).[format=integer]",
    "startDate": "The start date which records should be after this date.[format=YYYY-MM-DD]",
    "endDate": "The end date which records should be before this date.[format=YYYY-MM-DD]"
}
```

