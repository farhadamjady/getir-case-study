const Logger = require('../../utils/Logger');
const logger = new Logger();

/**
 *
 * @param response
 * @param data
 */
module.exports = function (response, data) {

	const STATUS_CODE = 200;
	const RESPONSE_CODE = 0;
	const ERROR_MESSAGE = 'Success';

	response.status(STATUS_CODE).json({
		code: RESPONSE_CODE,
		msg: ERROR_MESSAGE,
		...data
	});

};
