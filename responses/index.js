const defaultSuccess = require('./success/default');
const validationError = require('./error/validation');

module.exports = {
    success: {
        defaultSuccess
    },
    error: {
        validationError
    }
};
