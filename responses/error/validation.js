const Logger = require('../../utils/Logger');
const logger = new Logger();

/**
 *
 * @param response
 * @param debug
 */
module.exports = function (response, debug) {

	const STATUS_CODE = 422;
	const RESPONSE_CODE = 422;
	const ERROR_MESSAGE = 'VALIDATION_FAILED';

	response.status(STATUS_CODE).json({
		code: RESPONSE_CODE,
		msg: ERROR_MESSAGE,
		debug: debugMessage(debug)
	});

	/**
	 *
	 * @param err
	 * @returns {*}
	 */
	function debugMessage(err) {
		if (process.env.NODE_ENV !== 'PRODUCTION' ) {
			logger.log(err);
			return err;
		}
	}
};
