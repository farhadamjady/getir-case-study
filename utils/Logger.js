const winston = require('winston');

/**
 * Logger Class
 *
 */
class Logger {

    constructor () {
        this.instance = winston.createLogger({
            transports: [
                new winston.transports.Console()
            ]
        });
    }

    /**
     *
     * @param txt
     */
    log (txt) {
        if (process.env.NODE_ENV !== 'PRODUCTION' )
            this.instance.info(txt);
    }
}

module.exports = Logger;
