const Joi = require('Joi');
const validationError = require('../responses').error.validationError;

module.exports = function (req, res, next) {
	// define base schema
	const schemaFilters = {
		maxCount: Joi.number(),
		minCount: Joi.number(),
		startDate: Joi.date().iso(),
		endDate: Joi.date().iso()
	};
	// create schema object
	const schema = Joi.object(schemaFilters);

	// schema options
	const options = {
		abortEarly: false, // include all errors
		allowUnknown: true, // ignore unknown props
		stripUnknown: true // remove unknown props
	};

	// validate request body against schema
	const { error, value } = schema.validate(req.body, options);

	if (error) {
		validationError(res, `Validation error: ${error.details.map(x => x.message).join(', ')}`)
	} else {
		// on success replace req.body with validated value and trigger next middleware function
		req.body = value;
		next();
	}
};
