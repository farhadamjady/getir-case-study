require('dotenv').config();
const express = require('express');
var bodyParser = require('body-parser');
const Config = require('./Config');
const Logger = require('./utils/Logger');
const Routes = require('./routes/Routes');
const DataBaseProvider = require('./db/DataBaseProvider');

/**
 * The main class of application
 *
 */
class App {

	constructor() {
		this.app = express();
		this.app.use(bodyParser.urlencoded({ extended: false }));
		this.app.use(bodyParser.json());
		this.config = new Config();
		this.logger = new Logger();
		this.routes = new Routes(express, this.app);
		this.routes.initialRoutes();
	}

	/**
	 *
	 * @returns {Promise<void>}
	 */
	async start() {
		// Create db instance if its not TEST environment
		if (process.env.NODE_ENV !== 'TEST' ) {
			this.logger.log(`Initializing DB...`);
			this.dbProvider = new DataBaseProvider();
			await this.dbProvider.getInstance();
		}
		this.app.listen
		(
			this.config.getValue('APP_PORT'),
			this.config.getValue('APP_HOST'),
			() => {
				this.logger.log(`App is listening on PORT ${this.config.getValue('APP_PORT')}`);
			}
		)
	}

	getApp() {
		return this.app;
	}
}

module.exports = App;
