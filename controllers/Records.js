const RecordService = require('../services/Record');
const recordService = new RecordService();
const defaultSuccess = require('../responses').success.defaultSuccess;

/**
 * Handle Requests related to Records
 *
 */
class Records {

    /**
     *
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
    async getWithFilter(req, res) {
        const records = await recordService.findWithFilter(req.body);
        defaultSuccess(res, { records });
    }
}

module.exports = Records;
