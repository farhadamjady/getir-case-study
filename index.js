require('dotenv').config();
const App = require('./App');

// Initial the application
const app = new App();

// Start the application
app.start();
