const BaseModel = require('./BaseModel');

class RecordModel extends BaseModel{

    constructor() {
        super();
        this.collection = 'records';
    }

    /**
     *
     * @param filters
     * @returns {Promise<void>}
     */
    async findWithFilter(filters) {

        const instance = await this.getInstance();
        const match = {};

        // Check the filter params if exists
        if (filters.maxCount || filters.minCount) {
            match.totalCount = {};
            if (filters.maxCount) {
                match.totalCount['$lte'] = filters.maxCount;
            }
            if (filters.minCount) {
                match.totalCount['$gte'] = filters.minCount;
            }
        }

        if (filters.endDate || filters.startDate) {
            match.createdAt = {};
            if (filters.endDate) {
                match.createdAt['$lte'] = new Date(filters.endDate);
            }
            if (filters.startDate) {
                match.createdAt['$gte'] = new Date(filters.startDate);
            }
        }

        return await instance.db()
            .collection(this.collection)
            .aggregate([
                { $unwind: "$counts" },
                {
                    $group: {
                        _id: "$_id",
                        totalCount: { $sum: "$counts" },
                        key: { $first: "$key" },
                        createdAt: { $first: "$createdAt" }
                    }
                },
                {
                    $project: {
                        _id : 0 ,
                        totalCount : 1 ,
                        key : 1,
                        createdAt: 1
                    }
                },
                { $match: match }
            ]).toArray();
    }
}

module.exports = RecordModel;
