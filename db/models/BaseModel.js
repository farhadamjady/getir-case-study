const DataBaseProvider = require('../DataBaseProvider');

/**
 * The Base Model class
 *
 */
class BaseModel {

    constructor() {
        this.dbProvider = new DataBaseProvider();
    }

    /**
     *
     * @returns {Promise<*>}
     */
    async getInstance() {
        return await this.dbProvider.getInstance();
    }
}

module.exports = BaseModel;
