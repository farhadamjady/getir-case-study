const Config = require('../Config');

/**
 * DataBase provider class
 *
 */
class DataBaseProvider {

    constructor() {
        this.config = new Config();
        switch (this.config.getValue('DEFAULT_DB')) {
            case 'MONGO':
                this.dbInstance = require('./drivers/mongoDB');
            default:
                this.dbInstance = require('./drivers/mongoDB');
        }
    }

    /**
     * Get the singleton of DB instance
     *
     * @returns {Promise<*>}
     */
    async getInstance() {
        return await this.dbInstance.get();
    }
}

module.exports = DataBaseProvider;
