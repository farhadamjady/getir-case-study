const MongoClient = require('mongodb').MongoClient;
const Config = require('../../Config');

/**
 * MongoDB connection singleton
 *
 * @returns {{get: *}}
 * @constructor
 */
const DbConnection = function () {

	let db = null;
	const config = new Config();

	/**
	 *
	 * @returns {Promise<*>}
	 * @constructor
	 */
	async function DbConnect() {
		try {
			let url = config.mongoDb().url;
			return await MongoClient.connect(url, { useUnifiedTopology: true });
		} catch (error) {
			throw new Error(`MongoDB connection failed. error: ${error}`);
		}
	}

	/**
	 *
	 * @returns {Promise<*>}
	 * @constructor
	 */
	async function Get() {
		if (db != null) {
			return db;
		} else {
			db = await DbConnect();
			return db;
		}
	}

	return {
		get: Get
	}
};


module.exports = DbConnection();
