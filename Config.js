/**
 * The Config class
 *
 */
class Config {

	/**
	 *
	 * @returns {{url: (string|*)}}
	 */
	mongoDb() {
		return {
			url: this.getValue('MONGO_DB_CONNECTION_STRING')
		}
	}

	/**
	 * Get config key with default value
	 *
	 * @param key
	 * @param defaultValue
	 * @returns {string | *}
	 */
	getValue(key, defaultValue = null) {
		return process.env[key] || defaultValue;
	}
}

module.exports = Config;
