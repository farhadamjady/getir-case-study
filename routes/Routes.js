const fs = require('fs');
const path = require('path');
const Logger = require('../utils/Logger');

/**
 * Routes Initializer Class
 *
 */
class Routes {

    constructor(server, app) {
        this.app = app;
        this.server = server;
        this.logger = new Logger();
        this.counter = 0;
    }

    /**
     * Initial all routes based on route files in the directory
     *
     */
    initialRoutes() {
        let routeClasses = fs.readdirSync(path.join(__dirname));
        routeClasses
            .filter((routeFile) =>  routeFile !== 'Routes.js')
            .forEach((route) => {
                const routeClass = require(`./${route}`);
                const routeInstance = new routeClass(this.server);
                const router = routeInstance.initiate();
                this.app.use(`/${routeInstance.prefix}`, router);
                this.counter++;
            });
        this.logger.log(`${this.counter} Routes Initiated`);
    }
}

module.exports = Routes;
