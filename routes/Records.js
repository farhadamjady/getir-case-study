const schemas = require('../schemas');

/**
 * Routes related to records
 *
 */
class Records {

    /**
     *
     * @param app
     */
    constructor(app) {
        this.router = app.Router();
        this.prefix = 'records';
        this.controllerModule = require('../controllers/Records');
        this.controller = new this.controllerModule();
    }

    /**
     *
     * @returns {Router}
     */
    initiate() {
        this.router.post('/', schemas.filterRecords, this.controller.getWithFilter);
        return this.router;
    }
}

module.exports = Records;
