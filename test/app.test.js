const request = require("supertest");
const App = require("../app");
const RECORDS_PATH = "/records";
const mockRecords = require('../mockData/records');
const Config = require('../Config');
const config = new Config();
jest.mock('../db/models/Record', () => {
	return jest.fn().mockImplementation(() => {
		return { findWithFilter: (...args) => { return mockRecords(...args) } };
	});
});
let app = new App();
app = app.getApp();
let server;

beforeEach((done) => {
	server = app.listen(config.getValue('APP_PORT'), (err) => {
		if (err) return done(err);
		done();
	});
});

const requestFixture = req =>
	Object.assign(
		{},
		{
			minCount: 100,
			maxCount: 900,
			startDate: "2015-10-15",
			endDate: "2015-12-15"
		},
		req
	);

const requestRecords = (entity = requestFixture()) =>
	request(app)
		.post(RECORDS_PATH)
		.set("Accept", "application/json")
		.send(entity)
		.expect("Content-Type", /json/);

describe("app", () => {
	describe("When called with filters", () => {
		describe("When called with correct filters", () => {
			it("Accepted", async () => {
				return await requestRecords()
					.expect(200)
					.then(response => {
						expect(response.body.code).toBe(0);
						expect(response.body.records).toBeDefined();
						expect(response.body.records.length).toBe(mockRecords().length);
					});
			});
		});
	});
	describe("When the request is invalid", () => {
		describe("When the minCount is string", () => {
			it("Should return an error", async (done) => {
				await requestRecords(requestFixture({ minCount: "str" }))
					.expect(422)
					.then(response => {
						expect(response.body.code).toBe(422);
						expect(response.body.msg).toBe("VALIDATION_FAILED");
						expect(response.body.debug).toContain("minCount");
						done();
					});
			});
		});
		describe("When the maxCount is string", () => {
			it("Should return an error", async (done) => {
				await requestRecords(requestFixture({ maxCount: "str" }))
					.expect(422)
					.then(response => {
						expect(response.body.code).toBe(422);
						expect(response.body.msg).toBe("VALIDATION_FAILED");
						expect(response.body.debug).toContain("maxCount");
						done();
					});
			});
		});
		describe("When the startDate is null", () => {
			it("Should return an error", async (done) => {
				await requestRecords(requestFixture({ startDate: null }))
					.expect(422)
					.then(response => {
						expect(response.body.code).toBe(422);
						expect(response.body.msg).toBe("VALIDATION_FAILED");
						expect(response.body.debug).toContain("startDate");
						done();
					});
			});
		});
		describe("When the endDate is null", () => {
			it("Should return an error", async (done) => {
				await requestRecords(requestFixture({ endDate: null }))
					.expect(422)
					.then(response => {
						expect(response.body.code).toBe(422);
						expect(response.body.msg).toBe("VALIDATION_FAILED");
						expect(response.body.debug).toContain("endDate");
						done();
					});
			});
		});
	});
});

afterEach((done) => {
	return  server && server.close(done);
});
