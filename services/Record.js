const RecordModel = require('../db/models').Record;

/**
 * Record service class
 *
 */
class RecordService {

	/**
	 *
	 * @param data
	 * @returns {Promise<void>}
	 */
	async findWithFilter(data) {
		return await RecordModel.findWithFilter(data);
	}
}

module.exports = RecordService;
